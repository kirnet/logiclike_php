<?php

return [
    'banned_time' => 600,
    'ddos_time' => 60,
    'allow_visits_per_ddos_time' => 5
];