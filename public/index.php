<?php

require __DIR__ . '/../vendor/autoload.php';
$dbConfig = require_once __DIR__ . '/../config/db.php';
$params = require_once __DIR__ . '/../config/params.php';

$db = \ParagonIE\EasyDB\Factory::create(
    'mysql:host=localhost;dbname=' . $dbConfig['dbName'],
    $dbConfig['dbUser'],
    $dbConfig['dbPass']
);

spl_autoload_register(function ($class) {
    $baseDir =  __DIR__ . '/../src/';
    $file = $baseDir . str_replace('\\', '/', $class) . '.php';
    if (file_exists($file)) {
        require_once $file;
    }
});

$isDdos = false;
$app = new App($db, $params);

if ($app->isBanned()) {
    $app->banResponse();
}

$isDdos = $app->isDdos();
$rowId = $app->addClient($isDdos);

if ($isDdos === true) {
    $app->clearHistoryAfterBan($rowId);
    $app->banResponse();
}

echo 'Hello world!';




