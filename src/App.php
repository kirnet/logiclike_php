<?php

/**
 * Class App
 */
class App
{
    /** @var  ParagonIE\EasyDB\EasyDB*/
    private $db;

    /** @var array */
    private $params;

    /** @var string */
    private $clientIp;

    /** @var int  */
    public $banExpires = 0;

    /**
     * App constructor.
     *
     * @param $db
     * @param $params
     * @param string $clientIp
     */
    public function __construct($db, $params, $clientIp = '')
    {
        $this->db = $db;
        $this->params = $params;
        $this->clientIp = $clientIp ?: $_SERVER['REMOTE_ADDR'];
    }

    /**
     * @return int
     */
    public function countVisits()
    {
        $sql = "SELECT count(*) count FROM clients WHERE ip='{$this->clientIp}' AND ban_expires IS NULL";
        $data = $this->db->row($sql);
        return $data ? $data['count'] : 0;
    }

    /**
     * @param bool $isBanned
     *
     * @return int
     */
    public function addClient($isBanned = false)
    {
        $data = [
            'ip' => $this->clientIp,
            'time' => time()
        ];

        if ($isBanned) {
            $data['ban_expires'] = time() + $this->params['banned_time'];
        }
        $this->db->insert('clients', $data);
        return $this->db->lastInsertId();
    }

    /**
     * @return bool
     */
    public function isDdos(): bool
    {
        $countVisits = $this->countVisits();

        if ($countVisits < $this->params['allow_visits_per_ddos_time']) {
            return false;
        }

        $sql = "
          SELECT * 
          FROM clients 
          WHERE ip='{$this->clientIp}' 
          ORDER BY id DESC LIMIT " . ($this->params['allow_visits_per_ddos_time'])
        ;
        $data = $this->db->q($sql);
        $latest = array_shift($data);
        $first = end($data);
        return ($latest['time'] - $first['time']) <= $this->params['ddos_time'];
    }

    /**
     * @return bool
     */
    public function isBanned(): bool
    {
        $sql = "SELECT * FROM clients WHERE ip='{$this->clientIp}' AND ban_expires >=" . time();
        $data = $this->db->row($sql);
        if ($data !== null) {
            $this->banExpires = $data['ban_expires'];
        }
        return (bool) $data;
    }

    /**
     * @param $rowId
     *
     * @return int
     */
    public function clearHistoryAfterBan($rowId): int
    {
        $sql = "DELETE FROM clients WHERE ip='{$this->clientIp}' AND id < {$rowId}";
        return $this->db->exec($sql);
    }

    public function banResponse()
    {
        header('HTTP/1.1 403 Forbidden');
        header('Expires:' . date('D, d M Y H:i:s e', $this->banExpires));
        die;
    }
}